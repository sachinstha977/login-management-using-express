import express, { json } from "express";
import { port } from "./src/config.js";
import webUserRouter from "./src/router/webUserRouter.js";
import connectToMongoDB from "./src/databaseConnection/mongoDBConnection.js";

let expressApp = express();
expressApp.use(json()); // Always place this code at top of the router
expressApp.use(express.static("./public/"));

expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`);
});
connectToMongoDB();

expressApp.use("/web-users", webUserRouter);
